# espoir-openapi-api



## PROJET ESPOIR CHATGPT API

This project provides a Rest API to communicate with the ChatGPT API with synchronous communication (sending a question and receiving an answer).
This API was implemented by creating a microservice using Spring Boot


## HOW TO RUN THE CODE

-First Step
    Build a docker image executing this command: 
```
docker build -t chatgpt .
```

-Next Step
    You can run the docker image with the following command
```
docker run -p 8000:8090 chatgpt
```

## TOOLS

- Spring Boot
- Maven

