package de.bsi.openai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class ChatgptApplication {

	public static void main(String[] args)  {
		SpringApplication.run(ChatgptApplication.class, args);

	}

}
