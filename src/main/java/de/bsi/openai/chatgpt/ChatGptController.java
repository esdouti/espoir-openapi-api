package de.bsi.openai.chatgpt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import de.bsi.openai.FormInputDTO;
import de.bsi.openai.OpenAiApiClient;
import de.bsi.openai.OpenAiApiClient.OpenAiService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


@AllArgsConstructor
@NoArgsConstructor
@Controller
public class ChatGptController {
	
	private static final String MAIN_PAGE = "index";
	
	@Autowired private ObjectMapper jsonMapper;
	@Autowired private OpenAiApiClient client;


	
	private String chatWithGpt3(String message) throws Exception {
		var completion = CompletionRequest.defaultWith(message);
		var postBodyJson = jsonMapper.writeValueAsString(completion);
		var responseBody = client.postToOpenAiApi(postBodyJson, OpenAiService.GPT_3);
		var completionResponse = jsonMapper.readValue(responseBody, CompletionResponse.class);
		return completionResponse.firstAnswer().orElseThrow();
	}
	
	@GetMapping(path = "/")
	public String index() {
		return MAIN_PAGE;
	}


	@PostMapping(path = "/")
	public String chat(Model model, @ModelAttribute FormInputDTO dto) throws FileNotFoundException {
		try {
			model.addAttribute("request", dto.prompt());
			model.addAttribute("response", chatWithGpt3(dto.prompt()));
		} catch (Exception e) {
			model.addAttribute("response", "Error in communication with OpenAI ChatGPT API.");
		}


		var question1 = String.valueOf(model.getAttribute("request"));
		var response1 = String.valueOf(model.getAttribute("response"));


		String csvFile = "test.csv";
		CSVWriter writer;
		try {
			writer = new CSVWriter(new FileWriter(csvFile, true));
			String[] data = {question1, response1};
			writer.writeNext(data);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return MAIN_PAGE;
	}
	
}
